jfinal是由jfinal2.2+freemarker+easyui写的一个后台管理系统，参考了jty等一些优秀的开源项目，实现了一个基础的后台管理系统。这次更新整合了jfinal微信到后台管理系统。
在线访问：http://moriiy.wicp.net/jfinal/  有时可能无法访问（毕竟是部署在自己电脑上的），尽量白天访问，进入系统尽量不要做修改或删除操作。

启动类 cn.wawi.common.DefaultConfig

帐号：admin 密码：123456     测试 test  密码123456

系统截图


![](http://git.oschina.net/uploads/images/2015/1130/153213_4548160a_376262.png)

![](http://git.oschina.net/uploads/images/2015/1130/153204_7a934a91_376262.png)

![](http://git.oschina.net/uploads/images/2015/1130/153144_fa9c4a29_376262.png)